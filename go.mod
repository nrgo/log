module gitee.com/nrgo/log

go 1.15

require (
	bou.ke/monkey v1.0.2
	gitee.com/nrgo/moment v0.1.0
	github.com/smartystreets/goconvey v1.6.4
)

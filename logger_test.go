package log_test

import (
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"testing"
	"time"
	sys_time "time"

	"bou.ke/monkey"
	. "gitee.com/nrgo/log"
	"gitee.com/nrgo/moment"
	. "github.com/smartystreets/goconvey/convey"
)

type iw struct {
}

func (w *iw) Write(p []byte) (n int, err error) {
	return 0, nil
}

func TestSmokeBuiltin_Output(t *testing.T) {
	Convey("验证内建日志记录器的基本输出 Setup", t, func() {
		w := &iw{}
		l := New("log", LogDebug, w)
		l.SetOuput(w)
		l2 := New("log2", LogDebug)

		monkey.Patch(time.Now, func() sys_time.Time {
			t, _ := moment.From("2020/03/05 01:30:50 +0800 CST", "2006/01/02 15:04:05 -0700 MST")
			return t
		})

		Convey("验证 Lvl 设置", func() {
			// 单纯补充冒烟单测覆盖度
			l2.SetLevel(LogError)
			So(l2.Level(), ShouldEqual, LogError)
			l2.Debug("logcontent")
			l2.Debugf("%s", "logcontent")
		})

		Convey("验证 Debug 输出", func() {
			monkey.PatchInstanceMethod(reflect.TypeOf(w), "Write", func(_ *iw, p []byte) (int, error) {
				// [name] [Level] [2006-01-02 15:04:05.999999999 -0700 MST]
				So(fmt.Sprintf("[log] [DBUG] [%s] logcontent\n", moment.Format(time.Now(), "2006/01/02 15:04:05 -0700 MST")), ShouldEqual, string(p))
				return 0, nil
			})
			l.Debug("logcontent")
		})

		Convey("验证 Debugf 输出", func() {
			monkey.PatchInstanceMethod(reflect.TypeOf(w), "Write", func(_ *iw, p []byte) (int, error) {
				// [name] [Level] [2006-01-02 15:04:05.999999999 -0700 MST]
				So(fmt.Sprintf("[log] [DBUG] [%s] logcontent\n", moment.Format(time.Now(), "2006/01/02 15:04:05 -0700 MST")), ShouldEqual, string(p))
				return 0, nil
			})
			l.Debugf("%s", "logcontent")
		})

		Convey("验证 Info 输出", func() {
			monkey.PatchInstanceMethod(reflect.TypeOf(w), "Write", func(_ *iw, p []byte) (int, error) {
				// [name] [Level] [2006-01-02 15:04:05.999999999 -0700 MST]
				So(fmt.Sprintf("[log] [INFO] [%s] logcontent\n", moment.Format(time.Now(), "2006/01/02 15:04:05 -0700 MST")), ShouldEqual, string(p))
				return 0, nil
			})
			l.Info("logcontent")
		})

		Convey("验证 Infof 输出", func() {
			monkey.PatchInstanceMethod(reflect.TypeOf(w), "Write", func(_ *iw, p []byte) (int, error) {
				// [name] [Level] [2006-01-02 15:04:05.999999999 -0700 MST]
				So(fmt.Sprintf("[log] [INFO] [%s] logcontent\n", moment.Format(time.Now(), "2006/01/02 15:04:05 -0700 MST")), ShouldEqual, string(p))
				return 0, nil
			})
			l.Infof("%s", "logcontent")
		})

		Convey("验证 Warn 输出", func() {
			monkey.PatchInstanceMethod(reflect.TypeOf(w), "Write", func(_ *iw, p []byte) (int, error) {
				// [name] [Level] [2006-01-02 15:04:05.999999999 -0700 MST]
				So(fmt.Sprintf("[log] [WARN] [%s] logcontent\n", moment.Format(time.Now(), "2006/01/02 15:04:05 -0700 MST")), ShouldEqual, string(p))
				return 0, nil
			})
			l.Warn("logcontent")
		})

		Convey("验证 Warnf 输出", func() {
			monkey.PatchInstanceMethod(reflect.TypeOf(w), "Write", func(_ *iw, p []byte) (int, error) {
				// [name] [Level] [2006-01-02 15:04:05.999999999 -0700 MST]
				So(fmt.Sprintf("[log] [WARN] [%s] logcontent\n", moment.Format(time.Now(), "2006/01/02 15:04:05 -0700 MST")), ShouldEqual, string(p))
				return 0, nil
			})
			l.Warnf("%s", "logcontent")
		})

		Convey("验证 Error 输出", func() {
			monkey.PatchInstanceMethod(reflect.TypeOf(w), "Write", func(_ *iw, p []byte) (int, error) {
				// [name] [Level] [2006-01-02 15:04:05.999999999 -0700 MST]
				So(fmt.Sprintf("[log] [ERRO] [%s] logcontent\n", moment.Format(time.Now(), "2006/01/02 15:04:05 -0700 MST")), ShouldEqual, string(p))
				return 0, nil
			})
			l.Error("logcontent")
		})

		Convey("验证 Errorf 输出", func() {
			monkey.PatchInstanceMethod(reflect.TypeOf(w), "Write", func(_ *iw, p []byte) (int, error) {
				// [name] [Level] [2006-01-02 15:04:05.999999999 -0700 MST]
				So(fmt.Sprintf("[log] [ERRO] [%s] logcontent\n", moment.Format(time.Now(), "2006/01/02 15:04:05 -0700 MST")), ShouldEqual, string(p))
				return 0, nil
			})
			l.Errorf("%s", "logcontent")
		})

		Convey("验证 Fatal 输出", func() {
			monkey.PatchInstanceMethod(reflect.TypeOf(w), "Write", func(_ *iw, p []byte) (int, error) {
				// [name] [Level] [2006-01-02 15:04:05.999999999 -0700 MST]
				So(fmt.Sprintf("[log] [FTAL] [%s] logcontent\n", moment.Format(time.Now(), "2006/01/02 15:04:05 -0700 MST")), ShouldEqual, string(p))
				return 0, nil
			})
			l.Fatal("logcontent")
		})

		Convey("验证 Fatalf 输出", func() {
			monkey.PatchInstanceMethod(reflect.TypeOf(w), "Write", func(_ *iw, p []byte) (int, error) {
				// [name] [Level] [2006-01-02 15:04:05.999999999 -0700 MST]
				So(fmt.Sprintf("[log] [FTAL] [%s] logcontent\n", moment.Format(time.Now(), "2006/01/02 15:04:05 -0700 MST")), ShouldEqual, string(p))
				return 0, nil
			})
			l.Fatalf("%s", "logcontent")
		})
	})
}

func TestSmokeRotateWriter_Can_Rotate(t *testing.T) {
	Convey("验证 RotateWriter 是否可以正确运行", t, func() {
		fp, _ := os.Getwd()
		// root := filepath.Dir(fp)
		root := filepath.Join(fp, "testdata", "log", "f")
		root = filepath.Dir(root)

		w, _ := NewDailyRotateLogWriter(root, "ut")
		l := New("ut", LogDebug, w)

		Convey("验证 rotate 效果", func() {
			monkey.Patch(sys_time.Now, func() sys_time.Time {
				t, _ := moment.From("2020/03/05 01:30:50 +0800 CST", "2006/01/02 15:04:05 -0700 MST")

				return t
			})
			l.Debug("some log")
			l.Debug("some log2")

			_, errStat := os.Stat(filepath.Join(root, "ut2020-03-04.log"))
			So(errStat, ShouldBeNil)

			monkey.Patch(sys_time.Now, func() sys_time.Time {
				t, _ := moment.From("2020/03/06 01:30:50 +0800 CST", "2006/01/02 15:04:05 -0700 MST")
				return t
			})
			l.Debug("some log")

			_, errStat = os.Stat(filepath.Join(root, "ut2020-03-05.log"))
			So(errStat, ShouldBeNil)

			l2 := New("nraos", LogDebug)
			w2, _ := NewDailyRotateLogWriter(root, "ut")
			l2.SetOuput(w2)

			l2.Debug("some log for l2")
		})
	})
}

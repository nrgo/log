# log

#### 介绍
log interface for nrgo


#### 接口定义

```go
type Logger interface {
	Debug(v ...interface{})
	Debugf(format string, v ...interface{})
	Info(v ...interface{})
	Infof(format string, v ...interface{})
	Warn(v ...interface{})
	Warnf(format string, v ...interface{})
	Error(v ...interface{})
	Errorf(format string, v ...interface{})
	Fatal(v ...interface{})
	Fatalf(format string, v ...interface{})

	Log(lvl Level, v ...interface{})
	Logf(lvl Level, format string, v ...interface{})

	Level() Level
	SetLevel(l Level)

	SetOuput(out io.Writer)
}
```

#### 使用说明

```go
// New 初始化一个内建的日志记录器
func New(name string, lvl Level, out ...io.Writer) Logger

// NewDailyRotateLogWriter 创建一个按天滚动文件记录的 log writer
func NewDailyRotateLogWriter(logRoot, logName string) (io.Writer, error)

// demo
fp, _ := os.Getwd()
root := filepath.Join(fp, "testdata", "log", "f")
root = filepath.Dir(root)

w, _ := NewDailyRotateLogWriter(root, "ut")
l := New("ut", LogDebug, w)
```


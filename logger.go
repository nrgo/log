// Package log 提供 nraos 使用的标准日志记录接口、基本logger实现及dayrotate文件日志记录器
package log

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sync"
	"time"

	"gitee.com/nrgo/moment"
)

// Level 日志级别
type Level int

const (
	LogDebug Level = iota
	LogInfo
	LogWarning
	LogError
	LogFatal
	LogOff
	LogUnknown
)

var logLevelStr = []string{"DBUG", "INFO", "WARN", "ERRO", "FTAL", "OFF", "NONE"}

// Logger logger 接口
type Logger interface {
	Debug(v ...interface{})
	Debugf(format string, v ...interface{})
	Info(v ...interface{})
	Infof(format string, v ...interface{})
	Warn(v ...interface{})
	Warnf(format string, v ...interface{})
	Error(v ...interface{})
	Errorf(format string, v ...interface{})
	Fatal(v ...interface{})
	Fatalf(format string, v ...interface{})

	Log(lvl Level, v ...interface{})
	Logf(lvl Level, format string, v ...interface{})

	Level() Level
	SetLevel(l Level)

	SetOuput(out ...io.Writer)
}

var colorFormat = "\x1b[%dm%s\x1b[0m"
var logLevelColorCode = []int{33, 36, 35, 31, 41, 37, 37}

// builtin 内建基本 logger 实现
type builtin struct {
	mu   sync.Mutex
	lvl  Level
	name string
	out  []io.Writer
}

// New 初始化一个内建的日志记录器
func New(name string, lvl Level, out ...io.Writer) Logger {
	l := &builtin{
		name: name,
		lvl:  lvl,
	}

	if len(out) > 0 {
		l.out = out
	} else {
		l.out = append(make([]io.Writer, 0), os.Stdout)
	}

	return l
}

func (l *builtin) Level() Level {
	return l.lvl
}

func (l *builtin) SetLevel(lvl Level) {
	l.mu.Lock()
	l.lvl = lvl
	l.mu.Unlock()
}

func (l *builtin) SetOuput(out ...io.Writer) {
	l.mu.Lock()
	if len(out) > 0 {
		l.out = out
	} else {
		l.out = append(make([]io.Writer, 0), os.Stdout)
	}
	l.mu.Unlock()
}

func (l *builtin) Debug(v ...interface{}) {
	l.Log(LogDebug, v...)
}
func (l *builtin) Debugf(format string, v ...interface{}) {
	l.Logf(LogDebug, format, v...)
}
func (l *builtin) Info(v ...interface{}) {
	l.Log(LogInfo, v...)
}
func (l *builtin) Infof(format string, v ...interface{}) {
	l.Logf(LogInfo, format, v...)
}
func (l *builtin) Warn(v ...interface{}) {
	l.Log(LogWarning, v...)
}
func (l *builtin) Warnf(format string, v ...interface{}) {
	l.Logf(LogWarning, format, v...)
}
func (l *builtin) Error(v ...interface{}) {
	l.Log(LogError, v...)
}
func (l *builtin) Errorf(format string, v ...interface{}) {
	l.Logf(LogError, format, v...)
}
func (l *builtin) Fatal(v ...interface{}) {
	l.Log(LogFatal, v...)
}
func (l *builtin) Fatalf(format string, v ...interface{}) {
	l.Logf(LogFatal, format, v...)
}

func (l *builtin) Log(lvl Level, v ...interface{}) {
	if lvl < l.lvl {
		return
	}

	l.output(lvl, fmt.Sprintln(v...))
}
func (l *builtin) Logf(lvl Level, format string, v ...interface{}) {
	if lvl < l.lvl {
		return
	}

	l.output(lvl, fmt.Sprintf(format+"\n", v...)) // 木有找到 linebreak....看 xprintln 都是直接 \n的
}

func (l *builtin) prefix(w io.Writer, lvl Level) string {
	// [name] [Level] [2006/01/02 15:04:05 -0700 MST]
	return fmt.Sprintf("[%s] [%s] [%s]",
		l.name, l.colorizeLevel(w, lvl), moment.Format(time.Now(), "2006/01/02 15:04:05 -0700 MST"))
}

func (l *builtin) colorizeLevel(w io.Writer, lvl Level) string {
	if w == os.Stdout {
		return fmt.Sprintf(colorFormat, logLevelColorCode[lvl], logLevelStr[lvl])
	}

	return logLevelStr[lvl]
}

func (l *builtin) output(lvl Level, content string) {
	l.mu.Lock()
	defer l.mu.Unlock()

	// no check error
	for _, w := range l.out {
		_, _ = w.Write(([]byte)(fmt.Sprintf("%s %s", l.prefix(w, lvl), content)))
	}
}

// DailyRotateLogWriter nraos 提供的按天滚动文件记录的 log writer
type DailyRotateLogWriter struct {
	mu           sync.Mutex
	dailyPattern string
	out          *os.File
	logRoot      string
	logName      string
}

// NewDailyRotateLogWriter 创建一个按天滚动文件记录的 log writer
func NewDailyRotateLogWriter(logRoot, logName string) (io.Writer, error) {
	_, dp, out, err := rotateFile("", logRoot, logName)
	if err != nil {
		return nil, err
	}

	return &DailyRotateLogWriter{
		dailyPattern: dp,
		out:          out,
		logRoot:      logRoot,
		logName:      logName,
	}, nil
}

func (w *DailyRotateLogWriter) Write(p []byte) (n int, err error) {
	w.mu.Lock()
	defer w.mu.Unlock()

	w.rotate()

	return w.out.Write(p)
}

func (w *DailyRotateLogWriter) rotate() {
	same, dp, out, err := rotateFile(w.dailyPattern, w.logRoot, w.logName)

	if err != nil || same {
		// stop loop
		return
	}

	if w.out != nil {
		_ = w.out.Close()
	}

	w.dailyPattern = dp
	w.out = out
}

func dailyPattern() string {
	return moment.Format(time.Now().UTC(), moment.YYYYMMDD)
}

func dailyPath(logRoot, logName, dailyPattern string) (string, error) {
	path := filepath.Join(logRoot, fmt.Sprintf("%s%s.log", logName, dailyPattern))
	dir := filepath.Dir(path)

	if err := os.MkdirAll(dir, 0755); err != nil {
		return "", fmt.Errorf("failed to create directory %s", dir)
	}

	return path, nil
}

func attachToFile(path string) (*os.File, error) {
	_, errStat := os.Stat(path)
	f, err := os.OpenFile(path, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)

	if err != nil {
		return nil, fmt.Errorf("failed to open file %s: %s", path, err)
	}

	if errStat == nil {
		// log 文件已存在
		_, _ = f.Write([]byte("\n\n\n"))
	}

	return f, nil
}

func rotateFile(prePattern, logRoot, logName string) (bool, string, *os.File, error) {
	dp := dailyPattern()

	if dp == prePattern {
		return true, "", nil, nil
	}

	path, err := dailyPath(logRoot, logName, dp)

	if err != nil {
		return true, "", nil, err
	}

	out, err := attachToFile(path)

	if err != nil {
		return true, "", nil, err
	}

	return false, dp, out, err
}
